export interface Amount {
  amount: number;
  currency: string;
}

export interface Limit extends Amount {}

export interface CashInFee {
  percents: number;
  max: Limit;
}

export interface CashOutNaturalFee {
  percents: number;
  week_limit: Limit;
}

export interface CashOutJuridicalFee {
  percents: number;
  min: Limit;
}

export type Fees = CashInFee | CashOutNaturalFee | CashOutJuridicalFee;

export enum FeeType {
  CashInFee = "CashInFee",
  CashOutNaturalFee = "CashOutNaturalFee",
  CashOutJuridicalFee = "CashOutJuridicalFee",
}

export enum OperationType {
  cash_in = "cash_in",
  cash_out = "cash_out",
}

export enum UserType {
  juridical = "juridical",
  natural = "natural",
}

export type UserId = number;

export interface Operation {
  date: string;
  user_id: UserId;
  user_type: UserType;
  type: OperationType;
  operation: Amount;
}

export type GetFeeV2<T extends Fees> = (fee: T, op: Operation) => Amount;
export type FeeRateMap = {
  [type in OperationType]: {
    [userType in UserType]: FeeType;
  };
};

const feeRateMap: FeeRateMap = {
  [OperationType.cash_in]: {
    [UserType.juridical]: FeeType.CashInFee,
    [UserType.natural]: FeeType.CashInFee,
  },
  [OperationType.cash_out]: {
    [UserType.juridical]: FeeType.CashOutJuridicalFee,
    [UserType.natural]: FeeType.CashOutNaturalFee,
  },
};

export const resolveFeeType = (operation: Operation): FeeType | null => {
  const feeType = feeRateMap?.[operation.type]?.[operation.user_type];
  if (!feeType) {
    return null;
  }
  return feeType;
};
