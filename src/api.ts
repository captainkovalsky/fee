import AxiosInstance from "./axios.js";
import { CashInFee, CashOutJuridicalFee, CashOutNaturalFee } from "./types.js";

class ApiError {
  message: string;

  constructor(message: string) {
    this.message = message;
  }
}

export async function getCashInFee() {
  const uri = "https://developers.paysera.com/tasks/api/cash-in";

  return AxiosInstance.get<CashInFee>(uri)
    .then((response) => response.data)
    .catch(() => Promise.reject(new ApiError("failed to fetch cash-in fee")));
}

export async function getCashOutNaturalFee() {
  const uri = "https://developers.paysera.com/tasks/api/cash-out-natural";

  return AxiosInstance.get<CashOutNaturalFee>(uri)
    .then((response) => response.data)
    .catch(() =>
      Promise.reject(new ApiError("failed to fetch cash-out natural fee")),
    );
}

export async function getCashOutJuridicalFee() {
  const uri = "https://developers.paysera.com/tasks/api/cash-out-juridical";
  return AxiosInstance.get<CashOutJuridicalFee>(uri)
    .then((response) => response.data)

    .catch(() =>
      Promise.reject(new ApiError("failed to fetch cash-out juridical fee")),
    );
}
