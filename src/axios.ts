import axios from "axios";

export default axios.create({
  baseURL: "https://developers.paysera.com/tasks/api/",
  timeout: 2000,
});
