import { ReadStream } from "node:fs";
import { Transform, TransformOptions, TransformCallback } from "node:stream";
import { Amount, Operation } from "./types.js";
import { formatFee } from "./feeUtils.js";
import { IBranchCashier } from "./companyBranchCashier.js";

const [START_OBJ, END_OBJ] = ["{", "}"];

class JsonTransformer extends Transform {
  buffer = "";
  state = "init";
  openObjectTag = 0;

  constructor(options?: TransformOptions) {
    super(options);
  }

  _transform(
    chunk: any,
    _encoding: BufferEncoding,
    callback: TransformCallback,
  ) {
    const token: String = chunk.toString("utf8");

    if (token === " ") {
      callback();
      return;
    } else if (this.state === "init" && token === START_OBJ) {
      this.openObjectTag++;
      this.buffer += token;
      this.state = "reading";
    } else if (this.state === "reading") {
      this.buffer += token;
      if (token === START_OBJ) {
        this.openObjectTag++;
      }

      if (token === END_OBJ) {
        this.openObjectTag--;
      }

      if (this.openObjectTag === 0) {
        this.state = "endReading";
      }
    } else if (this.state === "endReading") {
      this.state = "init";
      try {
        const json = JSON.parse(this.buffer);
        this.buffer = "";
        this.push(json);
      } catch (e) {
        console.error("error", e);
        callback(new Error("failed to parse JSON"));
      }
    }

    callback();
  }
}

interface OperationReadable extends NodeJS.EventEmitter {
  on(
    event: "data",
    listener: (chunk: Operation, encoding: BufferEncoding) => void,
  ): this;
}

type FeeReadableImpl = OperationReadable & Transform;

export const getOperationsStream = (stream: ReadStream): FeeReadableImpl => {
  const toJsonTransformer = new JsonTransformer({ objectMode: true });
  return stream.pipe(toJsonTransformer);
};

export class CalculateFeeTransform extends Transform {
  constructor(private branchInfo: IBranchCashier) {
    super({ objectMode: true });
  }

  _transform(
    data: Operation,
    _encoding: BufferEncoding,
    callback: TransformCallback,
  ) {
    const fee = this.branchInfo.calculateFee(data);
    this.push(fee);
    callback();
  }
}

export class StdOutFormatFeeTransform extends Transform {
  constructor(_opts?: TransformOptions) {
    super({ objectMode: true });
  }

  _transform(
    fee: Amount,
    _encoding: BufferEncoding,
    callback: TransformCallback,
  ) {
    this.push(`${formatFee(fee)}\n`);
    callback();
  }
}
