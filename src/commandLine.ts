export const getArg = (): string | null => {
  let file = process.argv[2];

  if (file) {
    return `${process.cwd()}/${file}`;
  }

  return null;
};
