import { Amount } from "./types.js";

export const formatFee = (fee: Amount): string => {
  return fee.amount.toFixed(2);
};
