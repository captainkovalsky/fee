import {
  Amount,
  CashInFee,
  CashOutJuridicalFee,
  CashOutNaturalFee,
  FeeType,
  GetFeeV2,
  Operation,
  UserId,
  resolveFeeType,
} from "./types.js";
import { FeeTypeMap } from "./feeProvider.js";

interface UserOperation {
  total: number;
  date: Date;
}

export interface IBranchCashier {
  calculateFee(operation: Operation): Amount;
  getCashInFee: GetFeeV2<CashInFee>;
  getCashOutNaturalFee: GetFeeV2<CashOutNaturalFee>;
  getCashOutJuridicalFee: GetFeeV2<CashOutJuridicalFee>;
}

export enum LimitType {
  week = "week",
  // month = "month",
  // quarter = "quarter",
}
interface BranchConfig {
  limit: LimitType;
}

const round = (n: number): number => {
  if ((n * 100) % 1 === 0) {
    return n;
  }
  return Math.round(n * 100) / 100;
};

export default class CompanyBranchCashier implements IBranchCashier {
  private naturalCashOutOperations: Map<UserId, UserOperation[]> = new Map();

  private config: BranchConfig;

  constructor(
    private rates: FeeTypeMap,
    opts?: BranchConfig,
  ) {
    this.config = Object.assign({}, { limit: LimitType.week }, opts);
  }

  calculateFee(operation: Operation): Amount {
    const feeType = resolveFeeType(operation);
    if (!feeType) {
      throw new Error(`unknown fee type ${feeType}`);
    }

    const rate = this.rates[feeType];

    if (feeType === FeeType.CashInFee) {
      return this.getCashInFee(rate as CashInFee, operation);
    } else if (feeType === FeeType.CashOutNaturalFee) {
      return this.getCashOutNaturalFee(rate as CashOutNaturalFee, operation);
    } else {
      return this.getCashOutJuridicalFee(
        rate as CashOutJuridicalFee,
        operation,
      );
    }
  }

  getCashInFee(fee: CashInFee, op: Operation): Amount {
    const amount = round((fee.percents / 100) * op.operation.amount);
    if (amount > fee.max.amount) {
      return {
        amount: fee.max.amount,
        currency: op.operation.currency,
      };
    }

    return {
      amount,
      currency: op.operation.currency,
    };
  }

  getCashOutJuridicalFee(fee: CashOutJuridicalFee, op: Operation): Amount {
    const result = round((fee.percents / 100) * op.operation.amount);
    if (result < fee.min.amount) {
      return {
        amount: fee.min.amount,
        currency: op.operation.currency,
      };
    }
    return {
      amount: result,
      currency: op.operation.currency,
    };
  }

  //easy to get for any limit period, week, 2-w, 10-d, 3-m, 1-m
  getCurrentCashOutBalance(
    userId: UserId,
    period: { start: Date; end: Date },
  ): number {
    const userOperations = this.naturalCashOutOperations.get(userId) || [];
    const filteredOperations = userOperations.filter((operation) => {
      return operation.date >= period.start && operation.date <= period.end;
    });
    return filteredOperations.reduce(
      (sum, operation) => sum + operation.total,
      0,
    );
  }

  getPeriod(date: string, type: LimitType): { start: Date; end: Date } {
    const currentDate = new Date(date);
    let start: Date, end: Date;

    switch (type) {
      case LimitType.week:
        start = new Date(currentDate);
        const dayOfWeek = currentDate.getDay();
        const diff =
          currentDate.getDate() - dayOfWeek + (dayOfWeek === 0 ? -6 : 1);
        start.setDate(diff);
        end = new Date(start);
        end.setDate(start.getDate() + 6);
        break;
      default:
        throw new Error(`unable to calculate period for ${type}`); //just for case
    }

    return { start, end };
  }

  private addOperationToNaturalCashOut(op: Operation): void {
    const userOperations = this.naturalCashOutOperations.get(op.user_id) || [];
    userOperations.push({
      total: op.operation.amount,
      date: new Date(op.date),
    });
    this.naturalCashOutOperations.set(op.user_id, userOperations);
  }

  getCashOutNaturalFee(fee: CashOutNaturalFee, operation: Operation): Amount {
    const period = this.getPeriod(operation.date, this.config.limit);

    const noFee = { amount: 0, currency: operation.operation.currency };

    const currentCashOutBalance = this.getCurrentCashOutBalance(
      operation.user_id,
      period,
    );

    if (currentCashOutBalance < fee.week_limit.amount) {
      if (
        currentCashOutBalance + operation.operation.amount <
        fee.week_limit.amount
      ) {
        //still not reached
        this.addOperationToNaturalCashOut(operation);
        return noFee;
      } else {
        const exceededAmount =
          currentCashOutBalance +
          operation.operation.amount -
          fee.week_limit.amount;
        const feeAmount = round((fee.percents / 100) * exceededAmount);

        this.addOperationToNaturalCashOut(operation);

        return {
          amount: feeAmount,
          currency: operation.operation.currency,
        };
      }
    } else {
      //limit is reached anyway
      const feeAmount = round(
        (fee.percents / 100) * operation.operation.amount,
      );
      return {
        amount: feeAmount,
        currency: operation.operation.currency,
      };
    }
  }
}
