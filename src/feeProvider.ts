import {
  CashInFee,
  CashOutJuridicalFee,
  CashOutNaturalFee,
  FeeType,
} from "./types.js";
import {
  getCashInFee,
  getCashOutJuridicalFee,
  getCashOutNaturalFee,
} from "./api.js";

export type FeeTypeMap = {
  [FeeType.CashInFee]: CashInFee;
  [FeeType.CashOutNaturalFee]: CashOutNaturalFee;
  [FeeType.CashOutJuridicalFee]: CashOutJuridicalFee;
};

export type FeeFetcher = () => Promise<FeeTypeMap>;

export const apiFeeFetcher: FeeFetcher = async () => {
  const [CashInFee, CashOutNaturalFee, CashOutJuridicalFee] = await Promise.all(
    [getCashInFee(), getCashOutNaturalFee(), getCashOutJuridicalFee()],
  );

  return {
    [FeeType.CashInFee]: CashInFee,
    [FeeType.CashOutNaturalFee]: CashOutNaturalFee,
    [FeeType.CashOutJuridicalFee]: CashOutJuridicalFee,
  };
};
