import { stat } from "node:fs/promises";
import { getArg } from "./src/commandLine.js";
import {
  CalculateFeeTransform,
  StdOutFormatFeeTransform,
  getOperationsStream,
} from "./src/operationsStream.js";
import { apiFeeFetcher } from "./src/feeProvider.js";
import CompanyBranchCashier from "./src/companyBranchCashier.js";
import { createReadStream, ReadStream } from "node:fs";

const validateFile = async (filePath: string): Promise<[boolean, string?]> => {
  try {
    const stats = await stat(filePath);
    if (!stats.isFile()) {
      return [false, "invalid file"];
    }

    if (stats.size === 0) {
      return [false, "empty file"];
    }
  } catch (e) {
    return [false, "failed to open file"];
  }

  return [true];
};

export async function getPipeline(fileStream: ReadStream) {
  const rates = await apiFeeFetcher();
  const branchState = new CompanyBranchCashier(rates);
  const calculateFeeStream = new CalculateFeeTransform(branchState);
  const stdOutFormatFeeTransform = new StdOutFormatFeeTransform();
  return getOperationsStream(fileStream)
    .pipe(calculateFeeStream)
    .pipe(stdOutFormatFeeTransform);
}

async function main() {
  const filePath = getArg();
  if (!filePath) {
    console.error("please provide an input");
    return;
  }

  const [ok, message] = await validateFile(filePath);

  if (!ok) {
    console.error(`file issue: ${message}`);
    return;
  }

  const fileStream = createReadStream(filePath, {
    encoding: "utf8",
    highWaterMark: 1,
  });

  const pipeline = await getPipeline(fileStream);
  return pipeline.pipe(process.stdout);
}

try {
  await main();
} catch (e: any) {
  console.error(`[ERROR] ${e.message}`);
}
