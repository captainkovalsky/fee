Node

```
>=18.20.2
```

How to run?

```
yarn install

node dist/app.js input.json

```

Test

```
yarn test
```
