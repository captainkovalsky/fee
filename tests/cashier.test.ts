import { beforeEach, describe, it } from "node:test";
import assert from "node:assert";
import { Operation } from "../src/types.js";

import CompanyBranchCashier, {
  IBranchCashier,
} from "../src/companyBranchCashier.js";

const naturalOutFee = {
  percents: 0.3,
  week_limit: {
    amount: 1000,
    currency: "EUR",
  },
};

const legalOutFee = {
  percents: 0.3,
  min: {
    amount: 0.5,
    currency: "EUR",
  },
};

const cashInFee = {
  percents: 0.03,
  max: {
    amount: 5,
    currency: "EUR",
  },
};

const rates = {
  CashOutJuridicalFee: legalOutFee,
  CashInFee: cashInFee,
  CashOutNaturalFee: naturalOutFee,
};

describe("Company Cashier", () => {
  describe("Cash-in fee", () => {
    let branch: IBranchCashier;
    beforeEach(() => {
      branch = new CompanyBranchCashier(rates);
    });

    it("should calculate Cash-in Fee", () => {
      const operation = {
        date: "2016-01-05",
        user_id: 1,
        user_type: "natural",
        type: "cash_in",
        operation: { amount: 200.0, currency: "EUR" },
      } as Operation;

      const fee = branch.calculateFee(operation);

      assert.deepEqual(fee, {
        amount: 0.06,
        currency: "EUR",
      });
    });

    it("should calculate Cash-in Fee no more that 5 ", () => {
      const operation = {
        date: "2016-01-10",
        user_id: 2,
        user_type: "juridical",
        type: "cash_in",
        operation: { amount: 1000000.0, currency: "EUR" },
      } as Operation;
      const fee = branch.calculateFee(operation);

      assert.deepEqual(fee, {
        amount: 5,
        currency: "EUR",
      });
    });
  });

  describe("Cash-out fee", () => {
    describe("Natural Out Fee", () => {
      let branch: IBranchCashier;
      beforeEach(() => {
        branch = new CompanyBranchCashier(rates);
      });

      it("should calculate natural person fee", () => {
        const operation = {
          date: "2016-01-06",
          user_id: 1,
          user_type: "natural",
          type: "cash_out",
          operation: { amount: 30000, currency: "EUR" },
        } as Operation;

        const fee = branch.calculateFee(operation);

        assert.deepEqual(fee, {
          amount: 87.0,
          currency: "EUR",
        });
      });

      it("should calculate natural person fee when limit is not reached", () => {
        const operation = {
          date: "2016-01-07",
          user_id: 1,
          user_type: "natural",
          type: "cash_out",
          operation: { amount: 100.0, currency: "EUR" },
        } as Operation;

        assert.deepEqual(branch.calculateFee(operation), {
          amount: 0,
          currency: "EUR",
        });

        const operation2 = {
          date: "2016-01-08",
          user_id: 1,
          user_type: "natural",
          type: "cash_out",
          operation: { amount: 200.0, currency: "EUR" },
        } as Operation;

        assert.deepEqual(branch.calculateFee(operation2), {
          amount: 0,
          currency: "EUR",
        });
      });

      it("should calculate natural person fee when limit is reached", () => {
        const operation = {
          date: "2016-01-07",
          user_id: 1,
          user_type: "natural",
          type: "cash_out",
          operation: { amount: 100.0, currency: "EUR" },
        } as Operation;

        assert.deepEqual(branch.calculateFee(operation), {
          amount: 0,
          currency: "EUR",
        });

        const operation2 = {
          date: "2016-01-08",
          user_id: 1,
          user_type: "natural",
          type: "cash_out",
          operation: { amount: 200.0, currency: "EUR" },
        } as Operation;

        assert.deepEqual(branch.calculateFee(operation2), {
          amount: 0,
          currency: "EUR",
        });

        const operation3 = {
          date: "2016-01-09",
          user_id: 1,
          user_type: "natural",
          type: "cash_out",
          operation: { amount: 600.0, currency: "EUR" }, //limit is still not reached, total is 900
        } as Operation;

        assert.deepEqual(branch.calculateFee(operation3), {
          amount: 0,
          currency: "EUR",
        });

        const operation4 = {
          date: "2016-01-09",
          user_id: 1,
          user_type: "natural",
          type: "cash_out",
          operation: { amount: 200.0, currency: "EUR" }, //limit is reached, fee from 100
        } as Operation;

        assert.deepEqual(branch.calculateFee(operation4), {
          amount: 0.3,
          currency: "EUR",
        });
      });

      it("should calculate natural person fee only after limit", () => {
        const operation = {
          date: "2016-01-07",
          user_id: 1,
          user_type: "natural",
          type: "cash_out",
          operation: { amount: 1999, currency: "EUR" },
        } as Operation;

        assert.deepEqual(branch.calculateFee(operation), {
          amount: 3, // from 999
          currency: "EUR",
        });

        const operation2 = {
          date: "2016-01-08",
          user_id: 1,
          user_type: "natural",
          type: "cash_out",
          operation: { amount: 200.0, currency: "EUR" },
        } as Operation;

        assert.deepEqual(branch.calculateFee(operation2), {
          amount: 0.6,
          currency: "EUR",
        });
      });

      it("should reset natural person fee when week is over", () => {
        const operation = {
          date: "2016-01-07",
          user_id: 1,
          user_type: "natural",
          type: "cash_out",
          operation: { amount: 100.0, currency: "EUR" },
        } as Operation;

        assert.deepEqual(branch.calculateFee(operation), {
          amount: 0,
          currency: "EUR",
        });

        const operation2 = {
          date: "2016-01-08",
          user_id: 1,
          user_type: "natural",
          type: "cash_out",
          operation: { amount: 200.0, currency: "EUR" },
        } as Operation;

        assert.deepEqual(branch.calculateFee(operation2), {
          amount: 0,
          currency: "EUR",
        });

        const operation3 = {
          date: "2016-01-09",
          user_id: 1,
          user_type: "natural",
          type: "cash_out",
          operation: { amount: 700.0, currency: "EUR" }, //limit is still not reached, total is 1000
        } as Operation;

        assert.deepEqual(branch.calculateFee(operation3), {
          amount: 0,
          currency: "EUR",
        });

        const operation4 = {
          date: "2017-01-09", //next year)
          user_id: 1,
          user_type: "natural",
          type: "cash_out",
          operation: { amount: 200.0, currency: "EUR" }, //new week started
        } as Operation;

        assert.deepEqual(branch.calculateFee(operation4), {
          amount: 0,
          currency: "EUR",
        });
      });
    });

    describe("Legal Out Fee", () => {
      let branch: IBranchCashier;
      beforeEach(() => {
        branch = new CompanyBranchCashier(rates);
      });

      it("should calculate legal person fee", () => {
        const op = {
          date: "2016-01-06",
          user_id: 2,
          user_type: "juridical",
          type: "cash_out",
          operation: { amount: 300.0, currency: "EUR" },
        } as Operation;

        const fee = branch.calculateFee(op);

        assert.deepEqual(fee, {
          amount: 0.9,
          currency: "EUR",
        });
      });
    });
  });
});

//check
/*
0.06
0.90
87.00
90.00
90.30
90.60
5.00
0.00
0.00
* */
