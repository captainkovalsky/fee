import { readFile } from "node:fs/promises";
import { describe, it } from "node:test";
import assert from "node:assert";
import { Transform } from "node:stream";
import { getPipeline } from "../app.js";
import { createReadStream } from "node:fs";

class CaptureDataTransform extends Transform {
  private content: string = "";

  _transform(chunk: any, _encoding: BufferEncoding, callback: () => void) {
    this.content += chunk.toString();
    callback();
  }

  getContent(): string {
    return this.content.trim();
  }
}

describe("app", () => {
  it("should process transactions", async () => {
    const fileStream = createReadStream("./input.json", {
      encoding: "utf8",
      highWaterMark: 1,
    });

    const captureDataTransform = new CaptureDataTransform();

    const pipeline = await getPipeline(fileStream);
    pipeline.pipe(captureDataTransform);

    await new Promise((resolve) => pipeline.on("end", resolve));

    const expectedOutput = await readFile("./output.txt", "utf8");
    const capturedData = captureDataTransform.getContent();
    assert.deepEqual(capturedData, expectedOutput);
  });
});
